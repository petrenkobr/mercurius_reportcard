package com.example.petrenko.mercuriusreportcard.domain

/**
 * Created by Petrenko on 29/03/2017.
 */

data class Materia(
        var id: String,
        var nome: String,
        var professor: String,
        var sala: String,
        var obs: String,
        var cor: String,

        var nota_1b: String?,
        var nota_2b: String?,
        var notaPF: String?,
        var media_final: String?,


        var peso1B: Double,
        var peso2B: Double,
        var pesoMedias: Double,
        var pesoFinal : Double

        ) : MateriaItem()
