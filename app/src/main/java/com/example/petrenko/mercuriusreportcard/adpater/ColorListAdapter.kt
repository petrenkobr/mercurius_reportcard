package com.example.petrenko.mercuriusreportcard.adpater

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.petrenko.mercuriusreportcard.R
import com.example.petrenko.mercuriusreportcard.domain.Cor
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater

/**
 * Created by Petrenko on 04/04/2017.
 */

class ColorListAdapter(context: Context, val items: List<Cor>)
    : ArrayAdapter<Cor>(context, R.layout.spinner_item_color, R.id.dummy_text, items) {



    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = super.getView(position, convertView, parent)
        var item = getItem(position)
        var image = view.find<ImageView>(R.id.iv_cor)

        val drawable = image.drawable

        if (drawable is GradientDrawable) {
            drawable.setColor(Color.parseColor(item.cor))
        }

        return view
    }

    fun getPositionFor(cor: String) : Cor? {
        return items.firstOrNull { it.cor == cor }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = context.layoutInflater.inflate(R.layout.spinner_item_color_full, null)

        var item = getItem(position)
        var image = view.find<ImageView>(R.id.iv_cor)
        var txt = view.find<TextView>(R.id.tv_color)

        val drawable = image.drawable

        if (drawable is GradientDrawable) {
            drawable.setColor(Color.parseColor(item.cor))
        }
        txt.text = item.nome

        return view
    }

}