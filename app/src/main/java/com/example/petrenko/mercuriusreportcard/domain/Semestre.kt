package com.example.petrenko.mercuriusreportcard.domain

/**
 * Created by Petrenko on 22/03/2017.
 */

data class Semestre(
        var id : String,
        var cursoId : String,
        var nome: String,
        var cor: String,
        var numeroMaterias: Int
) : BoletimItem()