package com.example.petrenko.mercuriusreportcard.domain.dataObject

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*

/**
 * Created by Petrenko on 25/04/2017.
 */

open class MateriaData (

        //Informações Basicas
        @Required
        var nome: String = "",
        @Required
        var professor: String= "",
        @Required
        var sala: String= "",
        @Required
        var observacao: String= "",


        //1º Bimestre
        var hasNota1Bimestre1: Boolean = false,
        var nota1Bimestre1: Double = 0.0,

        var hasNota2Bimestre1: Boolean = false,
        var nota2Bimestre1: Double = 0.0,

        var hasNota1B: Boolean = false,
        var nota1B: String= "",

        var hasNota1Bimestre2: Boolean = false,
        var nota1Bimestre2: Double = 0.0,


        //2º Bimestre
        var hasNota2Bimestre2: Boolean = false,
        var nota2Bimestre2: Double = 0.0,

        var hasnotaMediaBimestres: Boolean = false,
        var notaMediaBimestres: Double = 0.0,

        var hasNota2B: Boolean = false,
        var nota2B: String= "",


        // P.F.
        var hasProvaFinal: Boolean = false,
        var notaProvaFinal: String= "",


        //Media Final
        var hasMediafinal: Boolean = false,
        var mediaFinal: String= "",


        //Pesos
        var pesoBimestre1: Double = 0.3,
        var pesoBimestre2: Double = 0.3,
        var pesoBimestres: Double = 0.4,
        var pesoPF: Double = 0.5,



        //Id
        @PrimaryKey
        var id: String = UUID.randomUUID().toString()
) : RealmObject()