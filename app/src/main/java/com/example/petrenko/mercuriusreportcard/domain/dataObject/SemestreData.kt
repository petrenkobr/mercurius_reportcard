package com.example.petrenko.mercuriusreportcard.domain.dataObject

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*

/**
 * Created by Petrenko on 25/04/2017.
 */
open class SemestreData(
        @Required
        var nome: String = "",
        var cursoId: String = "",
        var materias: RealmList<MateriaData> = RealmList<MateriaData>(),

        @PrimaryKey
        var id: String = UUID.randomUUID().toString()
) : RealmObject()