package com.example.petrenko.mercuriusreportcard


import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.InputType
import com.example.petrenko.mercuriusreportcard.adpater.BoletimListAdpater
import com.example.petrenko.mercuriusreportcard.adpater.ColorListAdapter
import com.example.petrenko.mercuriusreportcard.domain.*
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData
import com.example.petrenko.mercuriusreportcard.domain.dataObject.SemestreData
import com.example.petrenko.mercuriusreportcard.domain.extensions.toBoletimList
import com.vicpin.krealmextensions.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.onItemClick
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.petrenko.mercuriusreportcard.utils.showAlert
import io.realm.Case


class MainActivity : AppCompatActivity() {

    //region main
    companion object {
        val EDITAR_MENU_ID = 1
        val EXCLUIR_MENU_ID = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialize()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_novo_curso -> openDialogCurso()
            R.id.action_calcular -> navigateToCalcular()
            else -> return super.onOptionsItemSelected(item)

        }
        return true
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

        var info = menuInfo as AdapterView.AdapterContextMenuInfo
        var listView = v as ListView
        var itemSelected = listView.getItemAtPosition(info.position)

        if (itemSelected is Curso && menu != null) {
            var intent = Intent()
            intent.putExtra("cursoId", itemSelected.id)
            menu.add(1, EDITAR_MENU_ID, 1, "Editar Curso").intent = intent
            menu.add(1, EXCLUIR_MENU_ID, 2, "Excluir Curso").intent = intent

        } else if (itemSelected is Semestre && menu != null) {

            var intent = Intent()
            intent.putExtra("cursoId", itemSelected.cursoId)
            intent.putExtra("semestreId", itemSelected.id)
            menu.add(2, EDITAR_MENU_ID, 1, "Editar Semestre").intent = intent
            menu.add(2, EXCLUIR_MENU_ID, 2, "Excluir Semestre").intent = intent
        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        if (item != null && item.groupId == 1) {
            when (item.itemId) {
                EDITAR_MENU_ID -> editarCurso(item.intent.getStringExtra("cursoId"))
                EXCLUIR_MENU_ID -> excluirCurso(item.intent.getStringExtra("cursoId"))
            }
        }

        if (item != null && item.groupId == 2) {
            val id = item.intent.getStringExtra("semestreId")
            val cursoId = item.intent.getStringExtra("cursoId")
            when (item.itemId) {
                EDITAR_MENU_ID -> editarSemestre(id, cursoId)
                EXCLUIR_MENU_ID -> excluirSemestre(item.intent.getStringExtra("semestreId"))
            }
        }

        return super.onContextItemSelected(item)
    }

    fun initialize() {
        val lista = find<ListView>(R.id.lv_cursos)

        val items = CursoData().queryAll().sortedBy { it.nome }.toBoletimList()

        val adpater = BoletimListAdpater(this, items)

        lista.adapter = adpater

        lista.onItemClick { adapterView, view, position, id ->

            val item = adpater.getItem(position)

            if (item is Semestre) {
                val intent = Intent(this@MainActivity, SemestreActivity::class.java)
                intent.putExtra("id", item.id)
                intent.putExtra("cursoId", item.cursoId)
                startActivity(intent)
            }

        }

        registerForContextMenu(lista)

        adpater.onVerSemestre { id, cursoId ->

            val intent = Intent(this@MainActivity, SemestreActivity::class.java)
            intent.putExtra("id", id)
            intent.putExtra("cursoId", cursoId)
            startActivity(intent)

        }

        adpater.onAbrirSemestre { curso ->
            openSemestreDialog(curso.id)

        }
    }

    fun updateList() {

        val lista = find<ListView>(R.id.lv_cursos)
        val items = CursoData().queryAll().sortedBy { it.nome }.toBoletimList()
        val adapter = lista.adapter as BoletimListAdpater

        adapter.clear()
        adapter.addAll(items)
        adapter.notifyDataSetChanged()

    }
    //endregion

    //region Curso
    fun openDialogCurso(curso: CursoData? = null) {
        /*val builder = AlertDialog.Builder(this)
        builder.show()*/
        // mnesma coisa que de baixo

        val cores = Cores.all()

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_curso, null)
            val adapter = ColorListAdapter(this@MainActivity, cores)
            val spinner = view.find<Spinner>(R.id.spinner_cor)
            val nomeText = view.find<EditText>(R.id.et_curso)
            val notaMediaText = view.find<EditText>(R.id.et_nota_media)
            val notaMediaMinimaText = view.find<EditText>(R.id.et_nota_media_minima)
            val notaLimiteText = view.find<EditText>(R.id.et_nota_aprovacao)
            val textViewHeader = view.find<TextView>(R.id.tv_header)
            textViewHeader.text = "Adicionar Curso"


            spinner.adapter = adapter
            if (curso != null) {

                textViewHeader.text = "Editar Curso"
                 var cor = adapter.getPositionFor(curso.cor)
                spinner.setSelection(adapter.getPosition(cor), true)

                nomeText.setText(curso.nome)
                notaMediaText.setText(curso.notaMedia.toString())
                notaMediaMinimaText.setText(curso.notaMediaMinima.toString())
                notaLimiteText.setText(curso.notaLimite.toString())
            }

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }

            customView(view)
            positiveButton(if (curso != null) "Editar" else "Adicionar") {}

            show()
            val positiveButton = dialog!!.getButton(AlertDialog.BUTTON_POSITIVE)

            positiveButton.onClick {
                val item = spinner.selectedItem as Cor
                val nome = nomeText.text.toString()

                var notaMedia: Double = 0.0
                var notaMinima: Double = 0.0
                var notaLimite: Double = 0.0

                if (notaMediaText.text.toString() != "")
                    notaMedia = notaMediaText.text.toString().toDouble()

                if (notaMediaMinimaText.text.toString() != "")
                    notaMinima = notaMediaMinimaText.text.toString().toDouble()

                if (notaLimiteText.text.toString() != "") {
                    notaLimite = notaLimiteText.text.toString().toDouble()

                    if (curso != null) {
                        atualizarCurso(nome.trim().toUpperCase(), notaMedia, notaMinima, notaLimite, item.cor, curso)
                    } else {
                        adicionarCurso(nome.trim().toUpperCase(), notaMedia, notaMinima, notaLimite, item.cor)
                    }
                }

                if (nome.isEmpty()
                        || notaMedia.isNaN() || notaMedia <= 0
                        || notaMinima.isNaN() || notaMinima <= 0
                        || notaLimite.isNaN() || notaLimite <= 0) {

                    showAlert {

                    }

                } else {
                    dismiss()
                }
            }


        }
    }

    fun adicionarCurso(nome: String, notaMedia: Double, notaMinima: Double, notaLimite: Double, item: String) {

        val curso = CursoData(nome, notaMedia, notaMinima, notaLimite, item)

        if (verificarCurso(curso)) {
            curso.save()
            updateList()
        }

    }

    fun verificarCurso(curso: CursoData): Boolean {

        val displayCurso = curso.nome

        val sameName = CursoData().queryFirst { query -> query.equalTo("nome", curso.nome, Case.INSENSITIVE).notEqualTo("id", curso.id) }

        if (sameName != null) {
            alert {
                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)

                textViewAlerta.text = "Curso $displayCurso Já Cadastrado"

                view.find<LinearLayout>(R.id.ll_button_blue).visibility = View.VISIBLE
                val button =  view.find<Button>(R.id.btn_alert_action_blue)
                button.text = "OK"

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }
                button.onClick {
                    dialog!!.dismiss()
                }
                customView(view)
                show()
            }
            return false
        }

        if (curso.notaMedia < curso.notaMediaMinima) {
            alert {
                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
                textViewAlerta.text = "Média Deve Ser Maior que Média Mínima"

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }
                customView(view)
                show()
            }
            return false
        }

        return true
    }

    fun atualizarCurso(nome: String, notaMedia: Double, notaMinima: Double, notaLimite: Double, item: String, curso: CursoData) {

        curso.nome = nome
        curso.notaMediaMinima = notaMinima
        curso.notaMedia = notaMedia
        curso.notaLimite = notaLimite
        curso.cor = item

        if (verificarCurso(curso)) {
            curso.save()
            updateList()
        }
    }

    fun editarCurso(id: String) {

        val curso = CursoData().queryFirst { query -> query.equalTo("id", id) }

        openDialogCurso(curso)
    }

    fun excluirCurso(id: String) {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_alert, null)
            val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
            val buttonAlerta = view.find<Button>(R.id.btn_alert_action)

            textViewAlerta.text = "Esta Operação Não Poderá Ser Desfeita!"
            buttonAlerta.text = "Excluir Curso"
            view.find<LinearLayout>(R.id.ll_button_red).visibility = View.VISIBLE

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            view.find<Button>(R.id.btn_alert_action).onClick {
                CursoData().delete { query -> query.equalTo("id", id) }
                updateList()
                dialog!!.dismiss()
            }
            customView(view)
            show()
        }
    }

    //endregion

    //region Semestre
    private fun openSemestreDialog(cursoId: String, semestre: SemestreData? = null) {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_semestre, null)
            val textSemestre = view.find<EditText>(R.id.et_semestre)
            val textViewHeader = view.find<TextView>(R.id.tv_header)

            textViewHeader.text = "Adicionar Semestre"

            if (semestre != null) {
                textViewHeader.text = "Editar Semestre"
                textSemestre.setText(semestre.nome)
            }
            customView(view)
            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            positiveButton(if (semestre != null) "Editar" else "Adicionar") {}

            show()

            val positiveButton = dialog!!.getButton(AlertDialog.BUTTON_POSITIVE)

            positiveButton.onClick {

                textSemestre.clearFocus()
               // textSemestre.inputType = InputType.TYPE_NULL
              //  textSemestre.requestFocus()


                val nome = textSemestre.text.toString()

                if (nome == "") {
                    showAlert {

                    }
                } else {

                    if (semestre != null) {
                        alterarSemestre(nome, semestre, cursoId)

                    } else {
                        val nome = textSemestre.text.toString()
                        adicionarSemestre(nome, cursoId)

                    }
                    dismiss()
                }
            }

        }

    }

    fun adicionarSemestre(nome: String, cursoId: String) {

        val cursoData = CursoData().queryFirst { query -> query.equalTo("id", cursoId) }

        if (cursoData != null) {
            if (verificarSemestre(nome, cursoId)) {
                cursoData.semestres.add(SemestreData(nome, cursoId))
                cursoData.save()
            }
        }
        updateList()
    }

    fun verificarSemestre(semestreNovo: String, cursoId: String): Boolean {
        var retorno = true
        val querySemestre = SemestreData().queryFirst { query -> query.equalTo("cursoId", cursoId).equalTo("nome", semestreNovo) }

        if (querySemestre != null) {
            retorno = false
            alert {
                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)

                textViewAlerta.text = "$semestreNovo º Semestre Já Cadastrado Para Este Curso"

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }


                customView(view)
                show()
            }
        }
        return retorno
    }

    fun alterarSemestre(nome: String, semestre: SemestreData, cursoId: String) {

        semestre.nome = nome

        if (verificarSemestre(nome, cursoId)) {
            semestre.save()
        }
        updateList()
    }

    fun editarSemestre(id: String, cursoId: String) {

        val curso = CursoData().queryFirst { query -> query.equalTo("id", cursoId) }!!
        val semestre = curso.semestres.firstOrNull { it.id == id }

        if (semestre != null) {
            openSemestreDialog(curso.id, semestre)
        }
    }

    fun excluirSemestre(id: String) {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_alert, null)
            val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
            val buttonAlerta = view.find<Button>(R.id.btn_alert_action)

            textViewAlerta.text = "Esta Operação Não Poderá Ser Desfeita!"
            buttonAlerta.text = "Excluir Semestre"
            view.find<LinearLayout>(R.id.ll_button_red).visibility = View.VISIBLE

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            view.find<Button>(R.id.btn_alert_action).onClick {
                SemestreData().delete { query -> query.equalTo("id", id) }
                updateList()
                dialog!!.dismiss()
            }
            customView(view)
            show()
        }
    }
    //endregion

    fun navigateToCalcular() {
        startActivity(Intent(this@MainActivity, CalcularActivity::class.java))
    }

}

