package com.example.petrenko.mercuriusreportcard

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData
import com.example.petrenko.mercuriusreportcard.domain.dataObject.MateriaData
import com.example.petrenko.mercuriusreportcard.domain.dataObject.SemestreData
import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.save
import org.jetbrains.anko.*

class CalcularActivity : AppCompatActivity() {

    val materiaId by lazy { intent.getStringExtra("id") }
    val semestreId by lazy { intent.getStringExtra("semestreId") }
    val cursoId by lazy { intent.getStringExtra("cursoId") }

    val materia by lazy { MateriaData().queryFirst { query -> query.equalTo("id", materiaId) }!! }
    val semestre by lazy { SemestreData().queryFirst { query -> query.equalTo("id", semestreId) }!! }
    val curso by lazy { CursoData().queryFirst { query -> query.equalTo("id", cursoId) }!! }

    val hasMateria by lazy { MateriaData().queryFirst { query -> query.equalTo("id", materiaId) } != null }

    val textNota1B1 by lazy { find<EditText>(R.id.et_nota_1N1B) }
    val textNota2B1 by lazy { find<EditText>(R.id.et_nota_2N1B) }
    val textNota1B2 by lazy { find<EditText>(R.id.et_nota_1N2B) }
    val textNota2B2 by lazy { find<EditText>(R.id.et_nota_2N2B) }

    val textMediaB1 by lazy { find<TextView>(R.id.tv_nota_1b) }
    val textMediaB2 by lazy { find<TextView>(R.id.tv_nota_2b) }
    val textNotaPF by lazy { find<EditText>(R.id.et_nota_PF) }

    val textMediaBimestres by lazy { find<TextView>(R.id.tv_pf_media_bimetsres) }

    val textMediaFinal by lazy { find<TextView>(R.id.tv_media_final) }
    //val textMediaPF by lazy { find<TextView>(R.id.tv_nota_PF) }

    val textPesoN1B1 by lazy { find<TextView>(R.id.tv_porc_1N1B) }
    val textPesoN2B1 by lazy { find<TextView>(R.id.tv_porc_2N1B) }
    val textPesoN1B2 by lazy { find<TextView>(R.id.tv_porc_1N2B) }
    val textPesoN2B2 by lazy { find<TextView>(R.id.tv_porc_2N2B) }
    val textPesoB1 by lazy { find<TextView>(R.id.tv_porc_1b) }
    val textPesoB2 by lazy { find<TextView>(R.id.tv_porc_2b) }
    val textPesoPFMedias by lazy { find<TextView>(R.id.tv_pf_media_bimestres) }
    val textPesoPFNotaPF by lazy { find<TextView>(R.id.tv_pf_nota_pf) }
    //val textPesoPF by lazy { find<TextView>(R.id.tv_label_porc_prova_final) }


    val sliderPesoB1 by lazy { find<SeekBar>(R.id.sb_1b) }
    val sliderPesoB2 by lazy { find<SeekBar>(R.id.sb_2b) }
    val sliderMedia by lazy { find<SeekBar>(R.id.sb_media) }
    val sliderPesoFinal by lazy { find<SeekBar>(R.id.sb_prova_final) }

    val peso1 get() = (1 - (sliderPesoB1.progress.toDouble() / 100))
    val peso2 get() = (1 - (sliderPesoB2.progress.toDouble() / 100))
    val pesoMedia get() = (1 - (sliderMedia.progress.toDouble() / 100))
    val pesoFinal get() = (1 - (sliderPesoFinal.progress.toDouble() / 100))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calcula_media)

        if (hasMateria) {

            find<TextView>(R.id.tv_materia).text = materia.nome
            find<TextView>(R.id.tv_curso).text = curso.nome
            find<TextView>(R.id.tv_semestre).text = "${semestre.nome}º Semestre"

            if (materia.hasNota1Bimestre1) textNota1B1.setText(materia.nota1Bimestre1.toString())
            if (materia.hasNota2Bimestre1) textNota2B1.setText(materia.nota2Bimestre1.toString())
            if (materia.hasNota1Bimestre2) textNota1B2.setText(materia.nota1Bimestre2.toString())
            if (materia.hasNota2Bimestre2) textNota2B2.setText(materia.nota2Bimestre2.toString())

            if (materia.hasnotaMediaBimestres) textNotaPF.setText(materia.notaMediaBimestres.toString())

            sliderPesoB1.progress = ((1-materia.pesoBimestre1) * 100).toInt()
            sliderPesoB2.progress = ((1-materia.pesoBimestre2) * 100).toInt()
            sliderMedia.progress = ((1-materia.pesoBimestres) * 100).toInt()
            sliderPesoFinal.progress = ((1-materia.pesoPF) * 100).toInt()

            updateFields()
        }

        //region updateFields

        sliderPesoB1.onSeekBarChangeListener {
            onProgressChanged { seek, progress, i ->
                updateFields()
            }
        }

        sliderPesoB2.onSeekBarChangeListener {
            onProgressChanged { seek, progress, i ->
                updateFields()
            }
        }

        sliderMedia.onSeekBarChangeListener {
            onProgressChanged { seek, progress, i ->
                updateFields()
            }
        }

        sliderPesoFinal.onSeekBarChangeListener {
            onProgressChanged { seek, progress, i ->
                updateFields()
            }
        }

        textNota1B1.onFocusChange { view, b -> validateFields() }
        textNota1B1.textChangedListener {

            afterTextChanged {
                updateFields()
            }
        }

        textNota1B2.onFocusChange { view, b -> validateFields() }
        textNota1B2.textChangedListener {
            afterTextChanged {
                updateFields()
            }
        }

        textNota2B1.onFocusChange { view, b -> validateFields() }
        textNota2B1.textChangedListener {
            afterTextChanged {
                updateFields()
            }
        }

        textNota2B2.onFocusChange { view, b -> validateFields() }
        textNota2B2.textChangedListener {
            afterTextChanged {
                updateFields()
            }
        }

        //textNotaPF.onFocusChange { view, b -> if(!b)validateFields() }
        textNotaPF.textChangedListener {
            afterTextChanged {
                updateFields()
            }
        }

        //endregion

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        if (hasMateria && materia.observacao != "") {
            menuInflater.inflate(R.menu.menu_materia, menu)
        }


        return super.onCreateOptionsMenu(menu)
    }

    fun openObservacaoDialog() {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_obs, null)
            customView(view)
            val text = view.find<TextView>(R.id.tv_obs)
            text.text = materia.observacao
            okButton { }

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }

            show()

        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null && item!!.itemId == R.id.item_obs) {
            openObservacaoDialog()
        }

        return super.onOptionsItemSelected(item)
    }

    fun updateFields() {
        if(selfEditing) return
        try {
            var nota1b1 = textNota1B1.text.toString().toDoubleOrNull() ?: 0.0
            var nota1b2 = textNota1B2.text.toString().toDoubleOrNull() ?: 0.0
            var nota2b1 = textNota2B1.text.toString().toDoubleOrNull() ?: 0.0
            var nota2b2 = textNota2B2.text.toString().toDoubleOrNull() ?: 0.0
            var notaPF = textNotaPF.text.toString().toDoubleOrNull() ?: 0.0


            nota1b1 = Math.min(10.0, Math.max(nota1b1, 0.0))
            nota1b2 = Math.min(10.0, Math.max(nota1b2, 0.0))
            nota2b1 = Math.min(10.0, Math.max(nota2b1, 0.0))
            nota2b2 = Math.min(10.0, Math.max(nota2b2, 0.0))
            notaPF = Math.min(10.0, Math.max(notaPF, 0.0))

            val nota1b = textMediaB1.text.toString()
            val nota2b = textMediaB2.text.toString()
            val notaMediaBimestres = textMediaBimestres.toString()
            //val notaMediaBimestres = text.toString()
            val notaFinal = textMediaFinal.text.toString()

            val result = CalculadoraMedia().calcular(nota1b1, nota2b1, peso1,
                                                    nota1b2, nota2b2, peso2,
                                                    pesoMedia,
                                                    notaPF, pesoFinal,
                                                    curso)

            textMediaB1.text = "%.2f".format(result.mediaBimestre1)
            textMediaB2.text = "%.2f".format(result.mediaBimestre2)
            textMediaBimestres.text = "%.2f".format(result.mediaBimestres)

            // todo mudar esse resultado para a mediafinal

            if (materia.hasnotaMediaBimestres)textMediaFinal.text = "%.2f".format(result.mediaBimestres)
            //todo esta errado
            //if (materia.hasProvaFinal)
                textMediaFinal.text = "%.2f".format(result.mediaBimestres)

            textPesoN1B1.text = "${(peso1 * 100).toInt()}%"
            textPesoN2B1.text = "${((1 - peso1) * 100).toInt()}%"
            textPesoN1B2.text = "${(peso2 * 100).toInt()}%"
            textPesoN2B2.text = "${((1 - peso2) * 100).toInt()}%"

            textPesoB1.text = "${(pesoMedia * 100).toInt()}%"
            textPesoB2.text = "${((1 - pesoMedia) * 100).toInt()}%"

            textPesoPFMedias.text = "${(pesoFinal * 100).toInt()}%"
            textPesoPFNotaPF.text = "${((1 - pesoFinal) * 100).toInt()}%"

            val panelPF = find<LinearLayout>(R.id.ll_prova_final);
            panelPF.visibility = View.GONE



            if (hasMateria) {

                //todo
                // aqui tem curso
                if (result.mediaBimestres < curso.notaMediaMinima) {
                    textMediaFinal.textColor = resources.getColor(R.color.materialPink)
                } else if (result.mediaBimestres < curso.notaMedia) {
                    panelPF.visibility = View.VISIBLE
                    textMediaFinal.textColor = resources.getColor(R.color.materialPurple)

                }else{
                    textMediaFinal.textColor = resources.getColor(R.color.colorPrimaryDark)
                }


                if (textNota1B1.text.toString().trim() != "") {
                    materia.nota1Bimestre1 = nota1b1
                    materia.hasNota1Bimestre1 = true
                }
                if (textNota2B1.text.toString().trim() != "") {
                    materia.nota2Bimestre1 = nota2b1
                    materia.hasNota2Bimestre1 = true
                }

                if (textNota1B2.text.toString().trim() != "") {
                    materia.nota1Bimestre2 = nota1b2
                    materia.hasNota1Bimestre2 = true
                }
                if (textNota2B2.text.toString().trim() != "") {
                    materia.nota2Bimestre2 = nota2b2
                    materia.hasNota2Bimestre2 = true
                }
                if (textNotaPF.text.toString().trim() != "") {
                    materia.notaMediaBimestres = notaPF
                    materia.hasnotaMediaBimestres = true
                    //materia.hasProvaFinal = true
                }

                materia.pesoBimestre1 = peso1
                materia.pesoBimestre2 = peso2
                materia.pesoBimestres = pesoMedia
                materia.pesoPF = pesoFinal

                var p1 = Math.round(peso1 * 100.0)/100.0
                var p2 = Math.round(peso2 * 100.0)/100.0
                var pM = Math.round(pesoMedia * 100.0)/100.0
                var pF = Math.round(pesoFinal * 100.0)/100.0


                materia.pesoBimestre1 = p1
                materia.pesoBimestre2 = p2
                materia.pesoBimestres = pM
                materia.pesoPF = pF


                materia.nota1B = nota1b
                materia.nota2B = nota2b
                materia.notaMediaBimestres = notaPF
                materia.mediaFinal = notaFinal

                materia.save()
            }else{

                if (result.mediaBimestres < 4) {
                    textMediaFinal.textColor = resources.getColor(R.color.materialPink)
                } else if (result.mediaBimestres < 7) {
                    panelPF.visibility = View.VISIBLE
                    textMediaFinal.textColor = resources.getColor(R.color.materialPurple)
                } else {
                    textMediaFinal.textColor = resources.getColor(R.color.colorPrimaryDark)
                }
            }


        } catch (e: Exception) {
            resetFields()
        }

    }

    override fun onBackPressed() {

        updateFields()

        super.onBackPressed()
    }

    fun resetFields() {

    }

    var selfEditing: Boolean = false

    fun validateFields(){
        if(hasMateria){
            selfEditing  =true
            if (materia.hasNota1Bimestre1) textNota1B1.setText(materia.nota1Bimestre1.toString())
            if (materia.hasNota2Bimestre1) textNota2B1.setText(materia.nota2Bimestre1.toString())
            if (materia.hasNota1Bimestre2) textNota1B2.setText(materia.nota1Bimestre2.toString())
            if (materia.hasNota2Bimestre2) textNota2B2.setText(materia.nota2Bimestre2.toString())

            if (materia.hasnotaMediaBimestres) textNotaPF.setText(materia.notaMediaBimestres.toString())
            selfEditing =false
        }

    }

}
