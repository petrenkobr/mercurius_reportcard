package com.example.petrenko.mercuriusreportcard.domain

import com.example.petrenko.mercuriusreportcard.R

/**
 * Created by Petrenko on 04/04/2017.
 */

class Cor(
        var cor: String,
        var nome: String
)

object Cores {
    fun all(): List<Cor> {

        val materialYellow = "#ffeb3b"
        val materialPink = "#c2185b"
        val materialPurple = "#7e57c2"

        val materialBlue = "#3949ab"
        val materialGreen = "#00796b"
        val materialBrown = "#795548"

        val cores = listOf(
                Cor(materialYellow, "Amarelo"),
                Cor(materialPink, "Rosa"),
                Cor(materialPurple, "Roxo"),
                Cor(materialBlue, "Azul"),
                Cor(materialGreen, "Verde"),
                Cor(materialBrown, "Marron")
        )

        return cores

    }

}
