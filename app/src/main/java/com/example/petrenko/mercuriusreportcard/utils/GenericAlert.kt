package com.example.petrenko.mercuriusreportcard.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.example.petrenko.mercuriusreportcard.R
import org.jetbrains.anko.*

/**
 * Created by Nuvem on 5/3/2017.
 */

fun Activity.showAlert(action: () -> Unit) = this.showAlert(null, action)

fun Activity.showAlert(label: String? = null, action: () -> Unit) {

    val view = this.contentView!!.rootView.windowToken


    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager;
    imm.hideSoftInputFromWindow(view, 0)


    this.alert {
        val view = this@showAlert.layoutInflater.inflate(R.layout.dialog_alert, null)
        val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
        textViewAlerta.text = "É Necessario Informar Todos os Campos"



        view.find<ImageButton>(R.id.btn_close).onClick {
            dialog!!.dismiss()
        }

        customView(view)

        view.find<LinearLayout>(R.id.ll_button_blue).visibility = View.VISIBLE
        val btn = view.find<Button>(R.id.btn_alert_action_blue)
        btn.text = "OK"
        btn.onClick {
            dialog!!.dismiss()
        }


        show()
        dialog!!.setOnDismissListener {
            action()
        }
    }
}
