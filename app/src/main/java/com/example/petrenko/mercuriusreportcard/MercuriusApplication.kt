package com.example.petrenko.mercuriusreportcard

import android.app.Application
import io.realm.Realm

/**
 * Created by Petrenko on 25/04/2017.
 */

class MercuriusApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)

    }

}