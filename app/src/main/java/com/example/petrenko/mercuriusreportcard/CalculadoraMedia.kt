package com.example.petrenko.mercuriusreportcard

import com.example.petrenko.mercuriusreportcard.domain.Curso
import com.example.petrenko.mercuriusreportcard.domain.ResultadoCalculadora
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData

/**
 * Created by Petrenko on 08/04/2017.
 */


class CalculadoraMedia{

    fun calcular(nota1B1:Double, nota2B1:Double,peso1B1:Double,
                 nota1B2:Double, nota2B2:Double, peso1B2:Double,
                 pesoMedia:Double,
                 notaPF:Double, pesoFinal: Double,
                 curso:CursoData): ResultadoCalculadora {



        val contraPesoB1 = 1 - peso1B1
        val contraPesoB2 = 1 - peso1B2
        val contraPesoMedia = 1 - pesoMedia
        val contraPesoFinal = 1 - pesoFinal

        val mediaB1 = Math.max(0.0, Math.min( (nota1B1 * peso1B1) + (nota2B1 * contraPesoB1) , 10.0))
        val mediaB2 = Math.max(0.0,  Math.min((nota1B2 * peso1B2) + (nota2B2 * contraPesoB2), 10.0))


        var mediaProvaFinal = 0.0

        var mediaBimestres = Math.max(0.0, Math.min( (mediaB1 * pesoMedia) + (mediaB2 * contraPesoMedia), 10.0))

        if (mediaBimestres > curso.notaMediaMinima && mediaBimestres < curso.notaMedia) {
            mediaProvaFinal = Math.max(0.0, Math.min( (mediaBimestres * pesoFinal) + (notaPF * contraPesoFinal), 10.0))

        }
        return ResultadoCalculadora(mediaB1, mediaB2, mediaBimestres, mediaProvaFinal)

    }


}

