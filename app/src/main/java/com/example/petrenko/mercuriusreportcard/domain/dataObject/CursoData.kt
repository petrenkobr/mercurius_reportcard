package com.example.petrenko.mercuriusreportcard.domain.dataObject

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*

/**
 * Created by Petrenko on 25/04/2017.
 */

open class CursoData(
        @Required
        var nome: String = "",
        var notaMedia: Double = 0.0,
        var notaMediaMinima: Double = 0.0,
        var notaLimite: Double = 0.0,
        @Required
        var cor: String = "",

        var semestres: RealmList<SemestreData> = RealmList(),
        @PrimaryKey
        var id: String = UUID.randomUUID().toString()
) : RealmObject()