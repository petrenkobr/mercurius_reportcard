package com.example.petrenko.mercuriusreportcard.domain

/**
 * Created by Petrenko on 08/04/2017.
 */

data class ResultadoCalculadora(
        val mediaBimestre1: Double,
        val mediaBimestre2: Double,
        val mediaBimestres: Double,
        val mediaProvaFinal: Double
)