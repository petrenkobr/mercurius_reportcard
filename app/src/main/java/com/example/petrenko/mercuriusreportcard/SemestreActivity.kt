package com.example.petrenko.mercuriusreportcard

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.petrenko.mercuriusreportcard.adpater.MateriaListAdpater
import com.example.petrenko.mercuriusreportcard.domain.*
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData
import com.example.petrenko.mercuriusreportcard.domain.dataObject.MateriaData
import com.vicpin.krealmextensions.queryFirst
import com.vicpin.krealmextensions.save
import org.jetbrains.anko.*

class SemestreActivity : AppCompatActivity() {

    // region main

    companion object {
        val EDITAR_MENU_ID = 1
        val EXCLUIR_MENU_ID = 2
    }

    val cursoId by lazy { intent.getStringExtra("cursoId") }
    val id by lazy { intent.getStringExtra("id") }
    val curso by lazy { CursoData().queryFirst { query -> query.equalTo("id", cursoId) }!! }
    val semestre by lazy { curso.semestres.first { it.id == id } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_semestre)

        initialize()
    }

    fun updateList() {
        val lista = find<ListView>(R.id.lv_semestre)
        val items = mutableListOf<MateriaItem>()

        var curso = CursoData().queryFirst { query -> query.equalTo("id", cursoId) }!!
        val semes =  curso.semestres.first { it.id == id }

        items.addAll(semes.materias.map {
            Materia(it.id, it.nome, it.professor, it.sala, it.observacao,
                    curso.cor, it.nota1B, it.nota2B, it.notaMediaBimestres.toString(), it.mediaFinal,
                    it.pesoBimestre1, it.pesoBimestre2, it.pesoBimestres, it.pesoPF)
        })
        items.add(AdicionarMateria())

        val adapter = lista.adapter as MateriaListAdpater

        adapter.clear()
        adapter.addAll(items)
        adapter.notifyDataSetChanged()
    }

    fun initialize() {
        val borda = find<LinearLayout>(R.id.ll_borda_curso)
        val textMateria = find<TextView>(R.id.tv_materia)

        var textSemestre = semestre.nome

        if (" *[0-9]{1,} *".toRegex().matches(textSemestre)) {
            textSemestre += "º Semestre"
        }

        textSemestre += " - " + curso.nome

        textMateria.text = textSemestre
        borda.backgroundColor = Color.parseColor(curso.cor)

        val lista = find<ListView>(R.id.lv_semestre)
        val items = mutableListOf<MateriaItem>()

        items.addAll(semestre.materias.map {
            Materia(it.id, it.nome, it.professor, it.sala, it.observacao,
                    curso.cor, it.nota1B, it.nota2B, it.notaMediaBimestres.toString(), it.mediaFinal,
                    it.pesoBimestre1, it.pesoBimestre2, it.pesoBimestres, it.pesoPF)
        })

        items.add(AdicionarMateria())

        val adapter = MateriaListAdpater(this, curso.cor, items)

        lista.adapter = adapter

        lista.onItemClick { adapterView, view, position, _ ->

            var item = adapter.getItem(position)
            if (item is Materia) {
                openCalculaNotaMateria(item.id, curso.id, semestre.id)
            }
        }
        registerForContextMenu(lista)

        adapter.onAdicionarMateria {
            openNovaMaterialDialog(adapter)

        }

        adapter.onAbrirMateria { materiaId ->
        }
    }


    override fun onResume() {



        super.onResume()


    }


    override fun onPostResume() {

                updateList()

        super.onPostResume()


    }


    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

        var info = menuInfo as AdapterView.AdapterContextMenuInfo
        var listView = v as ListView
        var itemSelected = listView.getItemAtPosition(info.position)

        if (itemSelected is Materia && menu != null) {
            var intent = Intent()
            intent.putExtra("materiaId", itemSelected.id)
            menu.add(1, MainActivity.EDITAR_MENU_ID, 1, "Editar Matéria").intent = intent
            menu.add(1, MainActivity.EXCLUIR_MENU_ID, 2, "Excluir Matéria").intent = intent
        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        if (item != null && item.groupId == 1) {
            when (item.itemId) {
                MainActivity.EDITAR_MENU_ID -> editarMateria(item.intent.getStringExtra("materiaId"))
                MainActivity.EXCLUIR_MENU_ID -> excluirMateria(item.intent.getStringExtra("materiaId"))
            }
        }
        return super.onContextItemSelected(item)
    }
    //endregion

    //region materia
    fun openDialogMateria(materia: MateriaData? = null) {

        //val cores = Cores.all()

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_materia, null)
            val nomeText = view.find<EditText>(R.id.et_materia)
            val professorText = view.find<EditText>(R.id.et_professor)
            val salaText = view.find<EditText>(R.id.et_sala)
            val observacaoText = view.find<EditText>(R.id.et_observacao)

            val textViewHeader = view.find<TextView>(R.id.tv_materia_header)

            textViewHeader.text = "Adicionar Matéria"

            if (materia != null) {
                textViewHeader.text = "Editar Matéria"

                nomeText.setText(materia.nome)

                observacaoText.setText(materia.sala)
                professorText.setText(materia.professor)
                salaText.setText(materia.sala)
            }

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            customView(view)

            positiveButton(if (materia != null) "Editar" else "Adicionar") {}

            show()

            val positiveButton = dialog!!.getButton(AlertDialog.BUTTON_POSITIVE)

            positiveButton.onClick {
                val nome = nomeText.text.toString()
                val professor = professorText.text.toString()
                val sala = salaText.text.toString()
                val observacao = observacaoText.text.toString()

                //validar materia...

                if (verificarMateria(nome, materia!!.id)) {
                    dismiss()
                    if (materia != null) {
                        atualizarMateria(nome.trim().toUpperCase(), professor.trim(), sala.trim(), observacao, materia)
                    } else {
                        adicionarMateria(nome.trim().toUpperCase(), professor.trim(), sala.trim(), observacao)
                    }
                }

            }
        }
    }

    fun openNovaMaterialDialog(adapter: MateriaListAdpater) {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_materia, null)
            val nomeText = view.find<EditText>(R.id.et_materia)
            val profTExt = view.find<EditText>(R.id.et_professor)
            val salaText = view.find<EditText>(R.id.et_sala)
            val obsText = view.find<EditText>(R.id.et_observacao)
            customView(view)
            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            positiveButton("Adicionar") {

            }

            show()

            val positiveButton = dialog!!.getButton(AlertDialog.BUTTON_POSITIVE)

            positiveButton.onClick {

                if (verificarMateria(nomeText.text.toString())) {
                    dismiss()
                    adicionarMateria(
                            nomeText.text.toString(),
                            profTExt.text.toString(),
                            salaText.text.toString(),
                            obsText.text.toString()
                    )
                }
            }
        }
    }

    fun verificarMateria(materiaNova: String, materiaId: String? = null): Boolean {
        var retorno = true

        val materia = semestre.materias.firstOrNull { it.nome == materiaNova.toUpperCase().trim() && it.id != materiaId }
        //val queryMateria = MateriaData().queryFirst { query -> query.equalTo("cursoId", cursoId).equalTo("semestreId", semestreId).equalTo("nome", materiaNova) }

        if (materia != null) {
            retorno = false
            alert {
                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
                textViewAlerta.text = "A Disciplina $materiaNova Já Esta Cadastrada Para Este Semestre"


                view.find<LinearLayout>(R.id.ll_button_blue).visibility = View.VISIBLE
                val button = view.find<Button>(R.id.btn_alert_action_blue)
                button.text = "OK"

                view.find<Button>(R.id.btn_alert_action_blue).onClick {
                    dialog!!.dismiss()
                }

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }
                customView(view)
                show()
            }
        }

        if (materiaNova.toUpperCase().trim() == "") {
            retorno = false
            alert {

                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
                textViewAlerta.text = "O Campo 'Nome da Materia' Precisa Ser Informado"


                view.find<LinearLayout>(R.id.ll_button_blue).visibility = View.VISIBLE
                val button = view.find<Button>(R.id.btn_alert_action_blue)
                button.text = "OK"

                view.find<Button>(R.id.btn_alert_action_blue).onClick {
                    dialog!!.dismiss()
                }

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }
                customView(view)
                show()
            }
        }
        return retorno
    }

    fun adicionarMateria(nome: String, prof: String, sala: String, obs: String) {

        val materia = semestre.materias.firstOrNull { it.nome == nome.toUpperCase().trim() }
        if (materia == null) {

            semestre.materias.add(MateriaData(nome.toUpperCase().trim(), prof, sala, obs))
            semestre.save()

            updateList()
        } else {
            alert {
                val view = layoutInflater.inflate(R.layout.dialog_alert, null)
                val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
                textViewAlerta.text = "A Disciplina $nome Já Esta Cadastrada Para Este Semestre"

                view.find<LinearLayout>(R.id.ll_button_blue).visibility = View.VISIBLE
                val button = view.find<Button>(R.id.btn_alert_action_blue)
                button.text = "OK"

                view.find<Button>(R.id.btn_alert_action_blue).onClick {
                    dialog!!.dismiss()
                }

                view.find<ImageButton>(R.id.btn_close).onClick {
                    dialog!!.dismiss()
                }
                customView(view)
                show()
            }
        }
    }

    fun editarMateria(id: String) {
        val materia = MateriaData().queryFirst { query -> query.equalTo("id", id) }

        openDialogMateria(materia)
    }

    fun atualizarMateria(nome: String, professor: String, sala: String, observacao: String, m: MateriaData) {

        val materia = semestre.materias.first { it.id == m.id }

        materia.nome = nome
        materia.professor = professor
        materia.sala = sala
        materia.observacao = observacao

        semestre.save()

        updateList()
    }

    private fun excluirMateria(id: String) {

        alert {
            val view = layoutInflater.inflate(R.layout.dialog_alert, null)
            val textViewAlerta = view.find<TextView>(R.id.tv_alerta)
            val buttonAlerta = view.find<Button>(R.id.btn_alert_action)

            textViewAlerta.text = "Esta Operação Não Poderá Ser Desfeita!"
            buttonAlerta.text = "Excluir Matéria"
            view.find<LinearLayout>(R.id.ll_button_red).visibility = View.VISIBLE

            view.find<ImageButton>(R.id.btn_close).onClick {
                dialog!!.dismiss()
            }
            view.find<Button>(R.id.btn_alert_action).onClick {
                semestre.materias.removeAll { it.id == id }
                semestre.save()
                updateList()
                dialog!!.dismiss()
            }
            customView(view)
            show()
        }
    }
    //endregion

    fun openCalculaNotaMateria(materiaId: String, cursoId: String, semestreId: String) {

        val intent = Intent(this@SemestreActivity, CalcularActivity::class.java)
        intent.putExtra("id", materiaId)
        intent.putExtra("cursoId", cursoId)
        intent.putExtra("semestreId", semestreId)
        startActivity(intent)
    }
}




