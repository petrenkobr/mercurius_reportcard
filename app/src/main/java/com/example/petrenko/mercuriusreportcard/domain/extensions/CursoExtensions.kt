package com.example.petrenko.mercuriusreportcard.domain.extensions

import com.example.petrenko.mercuriusreportcard.R
import com.example.petrenko.mercuriusreportcard.domain.AdicionarSemestre
import com.example.petrenko.mercuriusreportcard.domain.BoletimItem
import com.example.petrenko.mercuriusreportcard.domain.Curso
import com.example.petrenko.mercuriusreportcard.domain.Semestre
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData
import com.vicpin.krealmextensions.queryAll

/**
 * Created by Petrenko on 25/04/2017.
 */


fun CursoData.toCurso() = Curso(
        this.id,
        this.nome,
        this.cor,
        this.notaMedia,
        this.notaMediaMinima,
        this.notaLimite
)

fun CursoData.getSemestres() = this.semestres.map { it.toSemestre(this) }

fun List<CursoData>.toBoletimList() : List<BoletimItem> {

    val cursos = this

    val mutableList = mutableListOf<BoletimItem>()

    cursos.forEach {
        val curso = it.toCurso()
        mutableList.add(curso)
        mutableList.addAll(it.getSemestres().sortedByDescending { it.nome.toInt() })
        mutableList.add(AdicionarSemestre(curso))
    }

    return mutableList

}