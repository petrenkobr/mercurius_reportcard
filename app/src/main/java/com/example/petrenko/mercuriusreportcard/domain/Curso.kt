package com.example.petrenko.mercuriusreportcard.domain

/**
 * Created by Petrenko on 22/03/2017.
 */

data class Curso (
        var id: String,
        var nome: String,
        var cor: String,
        var mediaCorteSemPF: Double,
        var mediaCortePF: Double,
        var notaAprovacao: Double
) : BoletimItem()
