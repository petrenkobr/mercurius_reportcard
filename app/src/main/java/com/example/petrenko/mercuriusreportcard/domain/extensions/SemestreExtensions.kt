package com.example.petrenko.mercuriusreportcard.domain.extensions

import com.example.petrenko.mercuriusreportcard.domain.Curso
import com.example.petrenko.mercuriusreportcard.domain.Semestre
import com.example.petrenko.mercuriusreportcard.domain.dataObject.CursoData
import com.example.petrenko.mercuriusreportcard.domain.dataObject.SemestreData

/**
 * Created by Petrenko on 25/04/2017.
 */


fun SemestreData.toSemestre(curso: CursoData) = Semestre(
        this.id,
        curso.id,
        this.nome,
        curso.cor,
        this.materias.size

)