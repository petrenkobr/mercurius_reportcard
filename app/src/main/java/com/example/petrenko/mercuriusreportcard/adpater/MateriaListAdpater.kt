package com.example.petrenko.mercuriusreportcard.adpater

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import com.example.petrenko.mercuriusreportcard.R
import com.example.petrenko.mercuriusreportcard.SemestreActivity
import com.example.petrenko.mercuriusreportcard.domain.*
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick
import org.jetbrains.anko.textColor

/**
 * Created by Petrenko on 29/03/2017.
 */


class MateriaListAdpater(context: Context, val cor: String, items: List<MateriaItem>)
//1º param é contexto(activity), 2º param layout padrão, 3º componente padrão
    : ArrayAdapter<MateriaItem>(context, R.layout.materia_details_listview_item, R.id.tv_materia, items) {


    var adicionarMateriaAction: () -> Unit = { }
    var abrirMateriaAction: (String) -> Unit = { }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = getItem(position)

        //if( item is Semestre) return getSemestreView(item, position, convertView, parent)
        if (item is AdicionarMateria) return getAddMateriaView(item, position, convertView, parent)

        val view = context.layoutInflater.inflate(R.layout.materia_details_listview_item, null)

        if (item is Materia) {

            view.find<TextView>(R.id.tv_materia).text = item.nome
            view.find<TextView>(R.id.tv_professor).text = item.professor
            view.find<TextView>(R.id.tv_sala).text = item.sala

            view.find<TextView>(R.id.tv_nota_1b).text = item.nota_1b
            view.find<TextView>(R.id.tv_nota_2b).text = item.nota_2b
            view.find<TextView>(R.id.tv_nota_PF).text = item.notaPF
            view.find<TextView>(R.id.tv_media_final).text = item.media_final
            val tvObs = view.find<TextView>(R.id.tv_obs)

            if(item.obs == ""){
                tvObs.setBackgroundResource(R.drawable.circle_empty)


            }else{
                tvObs.setBackgroundResource(R.drawable.circle)

                val background = tvObs.background as GradientDrawable

                background.setColor(Color.parseColor(item.cor))
            }


//todo
//            view.find<SeekBar>(R.id.sb_1b).progress = item.peso1B.toString().toIntOrNull() ?: 0
//            view.find<SeekBar>(R.id.sb_2b).progress = item.peso2B.toString().toIntOrNull() ?: 0
//            view.find<SeekBar>(R.id.sb_media).progress = item.pesoMedias.toString().toIntOrNull() ?: 0
//            view.find<SeekBar>(R.id.sb_prova_final).progress = item.pesoFinal.toString().toIntOrNull() ?: 0

        }
        return view
    }

    fun getAddMateriaView(item: AdicionarMateria, position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = context.layoutInflater.inflate(R.layout.materia_button_listview_item, null)

        var btn = view.find<Button>(R.id.btn_adicionar_materia)
        btn.textColor = Color.parseColor(cor)
        btn.onClick {
            adicionarMateriaAction()
        }

        return view
    }


    fun onAdicionarMateria(action: () -> Unit) {
        adicionarMateriaAction = action
    }

    fun onAbrirMateria(action: (String) -> Unit) {
        abrirMateriaAction = action
    }

}
