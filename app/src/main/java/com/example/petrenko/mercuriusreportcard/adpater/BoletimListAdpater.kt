package com.example.petrenko.mercuriusreportcard.adpater

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.example.petrenko.mercuriusreportcard.R
import com.example.petrenko.mercuriusreportcard.SemestreActivity
import com.example.petrenko.mercuriusreportcard.domain.*
import org.jetbrains.anko.*

/**
 * Created by Petrenko on 22/03/2017.
 */

class BoletimListAdpater (context: Context, items: List<BoletimItem>)
    : ArrayAdapter<BoletimItem>(context, R.layout.curso_header_listview_item, R.id.tv_curso, items){

    var abrirSemestreAction: (Curso)-> Unit = {i->}
    var verSemestreAction: (String, String)-> Unit = {i, j->}

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item =  getItem(position)

        if( item is Semestre) return getSemestreView(item, position, convertView, parent)
        if( item is AdicionarSemestre) return getAddSemestreView(item, position, convertView, parent)

        val view = context.layoutInflater.inflate(R.layout.curso_header_listview_item, null)

         if (item is Curso){
            view.find<TextView>(R.id.tv_curso).text = item.nome
            view.find<LinearLayout>(R.id.ll_borda_curso).backgroundColor = Color.parseColor(item.cor)
         }

        return view
    }
    fun getSemestreView(item: Semestre, position: Int, convertView: View?, parent: ViewGroup?): View{
        val view = context.layoutInflater.inflate(R.layout.curso_card_listview_item, null)
        view.find<LinearLayout>(R.id.ll_borda_curso).backgroundColor = Color.parseColor(item.cor)

        var textSemestre = item.nome

        if( " *[0-9]{1,} *".toRegex().matches(textSemestre) ){
            textSemestre += "º Semestre"
        }

        view.find<TextView>(R.id.tv_semestre).text = textSemestre
        view.find<TextView>(R.id.tv_quantidade).text = item.numeroMaterias.toString() + " Máterias"

        return view
    }

    fun getAddSemestreView(item: AdicionarSemestre, position: Int, convertView: View?, parent: ViewGroup?): View{
        val view = context.layoutInflater.inflate(R.layout.curso_button_listview_item, null)

        val btn = view.find<Button>(R.id.btn_semestre)

        view.find<LinearLayout>(R.id.ll_borda_curso).backgroundColor = Color.parseColor(item.curso.cor)

        btn.textColor =  Color.parseColor(item.curso.cor)
        btn.onClick {
            abrirSemestreAction(item.curso)
        }

        return view
    }

    fun onAbrirSemestre(action: (Curso)-> Unit){
        abrirSemestreAction = action

    }


    fun onVerSemestre(action: (String, String) -> Unit){
        verSemestreAction = action
    }

}
